package kz.attractor.todolist.repository;

import kz.attractor.todolist.model.Task;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface TaskRepository extends MongoRepository<Task, String> {
    List<Task> findByOrderByNameAsc();
    List<Task> findByOrderByNameDesc();
    List<Task> findByOrderByPriorityAsc();
    List<Task> findByOrderByPriorityDesc();
    List<Task> findByOrderByStatusAsc();
    List<Task> findByOrderByStatusDesc();
    List<Task> findByOrderByCreationTimeAsc();
    List<Task> findByOrderByCreationTimeDesc();
}
