package kz.attractor.todolist.controller;

import kz.attractor.todolist.model.Task;
import kz.attractor.todolist.repository.TaskRepository;
import kz.attractor.todolist.service.SortOrder;
import kz.attractor.todolist.service.TaskService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/tasks")
public class TaskController {
    private final TaskRepository taskRepository;
    private final TaskService taskService;

    public TaskController(TaskRepository taskRepository, TaskService taskService) {
        this.taskRepository = taskRepository;
        this.taskService = taskService;
    }

    @GetMapping
    public String get(Model model,
                      @RequestParam (defaultValue = "ByNameAsc", required = false) SortOrder order) {
        List<Task> tasks = taskService.sortTasks(order);
        taskService.addSortModels(model, order);
        model.addAttribute("tasks", tasks);
        return "tasks";
    }

    @GetMapping("/{id}")
    public String getTask(Model model, @PathVariable String id) {
        Task task = taskService.getById(id);
        model.addAttribute("task", task);
        return "task";
    }

    @GetMapping("/{id}/delete")
    public String getDelete(Model model, @PathVariable String id) {
        Task task = taskService.getById(id);
        model.addAttribute("task", task);
        return "delete";
    }

    @GetMapping("/add-task")
    public String getAdd() {
        return "add-task";
    }

    @PostMapping
    public String updateStatus(@RequestParam String id) {
        taskService.updateTaskStatus(id);
        return "redirect:/tasks";
    }

    @PostMapping("/{id}/delete")
    public String delete(@PathVariable String id) {
        taskRepository.deleteById(id);
        return "redirect:/tasks";
    }

    @PostMapping("/add-task")
    public String addTask(@RequestParam String name,
                          @RequestParam String executorName,
                          @RequestParam String priority,
                          @RequestParam String description) {
        taskService.addTask(name, description, priority, executorName);
        return "redirect:/tasks";
    }
}
