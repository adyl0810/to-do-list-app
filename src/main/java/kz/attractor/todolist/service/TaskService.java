package kz.attractor.todolist.service;

import kz.attractor.todolist.model.Task;
import kz.attractor.todolist.repository.TaskRepository;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class TaskService {
    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task getById(String id) {
        return taskRepository.findById(id).orElseThrow(() ->
                new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        String.format("Задача с id %s не найдена", id)
                ));
    }

    public void addTask(String name, String description, String priority, String executorName) {
        Task task = new Task(name, description, Priority.valueOf(priority), executorName);
        task.setCreationTime(LocalDateTime.now());
        task.setStatus(Status.NEW);
        taskRepository.save(task);
    }

    public void updateTaskStatus(String id) {
        Task task = getById(id);
        switch (task.getStatus()) {
            case NEW:
                task.setStatus(Status.OPEN);
                task.setStartTime(LocalDateTime.now());
                break;
            case OPEN:
                task.setStatus(Status.CLOSED);
                task.setEndTime(LocalDateTime.now());
                break;
        }
        taskRepository.save(task);
    }

    public void addSortModels(Model model, SortOrder order) {
        model.addAttribute("ByName", byName(order));
        model.addAttribute("ByPriority", byPriority(order));
        model.addAttribute("ByStatus", byStatus(order));
        model.addAttribute("ByCreationDate", byCreationDate(order));
    }

    public List<Task> sortTasks(SortOrder order) {
        switch (order) {
            case ByNameAsc:
                return taskRepository.findByOrderByNameAsc();
            case ByNameDesc:
                return taskRepository.findByOrderByNameDesc();
            case ByPriorityAsc:
                return taskRepository.findByOrderByPriorityAsc();
            case ByPriorityDesc:
                return taskRepository.findByOrderByPriorityDesc();
            case ByStatusAsc:
                return taskRepository.findByOrderByStatusAsc();
            case ByStatusDesc:
                return taskRepository.findByOrderByStatusDesc();
            case ByCreationTimeAsc:
                return taskRepository.findByOrderByCreationTimeAsc();
            case ByCreationTimeDesc:
                return taskRepository.findByOrderByCreationTimeDesc();
            default: return taskRepository.findAll();
        }
    }

    private SortOrder byName(SortOrder order) {
        if (order == SortOrder.ByNameAsc) {
            return SortOrder.ByNameDesc;
        }
        return SortOrder.ByNameAsc;
    }

    private SortOrder byPriority(SortOrder order) {
        if (order == SortOrder.ByPriorityAsc) {
            return SortOrder.ByPriorityDesc;
        }
        return SortOrder.ByPriorityAsc;
    }

    private SortOrder byStatus(SortOrder order) {
        if (order == SortOrder.ByStatusAsc) {
            return SortOrder.ByStatusDesc;
        }
        return SortOrder.ByStatusAsc;
    }

    private SortOrder byCreationDate(SortOrder order) {
        if (order == SortOrder.ByCreationTimeAsc) {
            return SortOrder.ByCreationTimeDesc;
        }
        return SortOrder.ByCreationTimeAsc;
    }
    
}
