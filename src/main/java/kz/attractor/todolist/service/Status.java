package kz.attractor.todolist.service;

public enum Status {

    NEW("Новая"),
    OPEN("Открыта"),
    CLOSED("Закрыта");

    private final String name;

    Status(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
