package kz.attractor.todolist.service;

public enum Priority {

    HIGH("Высокий"),
    MID("Средний"),
    LOW("Низкий");

    private final String name;

    Priority(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
