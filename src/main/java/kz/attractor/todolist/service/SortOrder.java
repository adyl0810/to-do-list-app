package kz.attractor.todolist.service;

public enum SortOrder {
    ByNameAsc,
    ByNameDesc,
    ByPriorityAsc,
    ByPriorityDesc,
    ByStatusAsc,
    ByStatusDesc,
    ByCreationTimeAsc,
    ByCreationTimeDesc,
    Empty;
}
