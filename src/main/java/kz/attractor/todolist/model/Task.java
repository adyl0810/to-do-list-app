package kz.attractor.todolist.model;

import kz.attractor.todolist.service.Priority;
import kz.attractor.todolist.service.Status;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@Document(collection = "tasks")
public class Task {

    @Id
    private String id;
    private String name;
    private String description;
    private Priority priority;
    private Status status;
    private String executorName;
    private LocalDateTime creationTime;
    private LocalDateTime startTime;
    private LocalDateTime endTime;

    public Task(String name, String description, Priority priority, String executorName) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.description = description;
        this.priority = priority;
        this.executorName = executorName;
    }
}
